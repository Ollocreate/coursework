from django.db import models
from restraunt.models import Restraunt
from authentication.models import User


class Review(models.Model):
  author = models.ForeignKey(verbose_name='Автор', to=User, on_delete=models.CASCADE)
  text = models.TextField(verbose_name='Текст отзыва', max_length=255)
  assess = models.IntegerField(verbose_name='Рейтинг')
  date = models.DateTimeField(verbose_name='Дата публикации', max_length=255)
  restraunt = models.ForeignKey(verbose_name='Ресторан', to=Restraunt, on_delete=models.CASCADE)


  def __str__(self):
    return self.text

  class Meta:
    verbose_name = 'Отзыв'
    verbose_name_plural = 'Отзывы'