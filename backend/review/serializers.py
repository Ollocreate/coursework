from rest_framework import serializers
from review.models import Review
from authentication.models import User
from restraunt.models import Restraunt

class AuthorSerializerForReview(serializers.ModelSerializer):
  class Meta:
    model = User
    fields = ['name', ]

class RestrauntSerializerForReview(serializers.ModelSerializer):
  class Meta:
    model = Restraunt
    fields = ['title', ]

class ReviewSerializer(serializers.ModelSerializer):
  author_data = AuthorSerializerForReview(source="author")
  restraunt_data = RestrauntSerializerForReview(source="restraunt")
  class Meta:
    model = Review
    exclude = ['author', 'restraunt', ]
