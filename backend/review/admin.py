from django.contrib import admin
from review.models import Review


class ReviewAdmin(admin.ModelAdmin):
    list_display = ('id', 'author', 'restraunt', 'date')
    list_display_links = ('id', 'author', 'restraunt')
    search_fields = ('id', 'text', 'author', 'restraunt')
    list_filter = ('date',)


admin.site.register(Review, ReviewAdmin)