from django.contrib import admin
from dish.models import Dish


class DishAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'price', 'restraunt')
    list_display_links = ('id', 'title')
    search_fields = ('id', 'title', 'ingredients', 'restraunt')


admin.site.register(Dish, DishAdmin)