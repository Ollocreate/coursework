from rest_framework.viewsets import ModelViewSet
from dish.serializers import DishSerializer
from dish.models import Dish


class DishViewSet(ModelViewSet):
    queryset = Dish.objects.all()
    serializer_class = DishSerializer
