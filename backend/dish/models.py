from django.db import models
from restraunt.models import Restraunt


class Dish(models.Model):
  title = models.CharField(verbose_name='Название', max_length=255)
  restraunt = models.ForeignKey(verbose_name='Ресторан', to=Restraunt, on_delete=models.CASCADE)
  ingredients = models.TextField(verbose_name='Ингредиенты')
  price = models.FloatField(verbose_name='Цена', max_length=255)
  type = models.CharField(verbose_name='Тип', max_length=255)
  photo = models.ImageField(verbose_name='Фото товара', upload_to='dishes')


  def __str__(self):
    return self.title

  class Meta:
    verbose_name = 'Блюдо'
    verbose_name_plural = 'Блюда'