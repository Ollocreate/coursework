from django.contrib import admin
from authentication.models import User


class UserAdmin(admin.ModelAdmin):
    list_display = ('id', 'email', 'name', 'is_active')
    list_display_links = ('id', 'name')
    search_fields = ('id', 'email', 'name', 'address')
    list_filter = ('is_active', 'is_staff')
    list_editable = ('is_active',)


admin.site.register(User, UserAdmin)