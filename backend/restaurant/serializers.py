from rest_framework import serializers
from restraunt.models import Restraunt
from category.models import Category

class CategorySerializerForRestraunts(serializers.ModelSerializer):
  class Meta:
    model = Category
    fields = ['title', ]

class RestrauntSerializer(serializers.ModelSerializer):
  categories_data = CategorySerializerForRestraunts(source="category")
  class Meta:
    model = Restraunt
    exclude = ['category', ]