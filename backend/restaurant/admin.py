from django.contrib import admin
from restraunt.models import Restraunt


class RestrauntAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'rate', 'cost_of_delivery')
    list_display_links = ('id', 'title')
    search_fields = ('id', 'title', 'description', 'cost_of_delivery', 'category')
    list_filter = ('rate', 'time_of_delivery')


admin.site.register(Restraunt, RestrauntAdmin)