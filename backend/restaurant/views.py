from rest_framework.viewsets import ModelViewSet
from restraunt.serializers import RestrauntSerializer
from restraunt.models import Restraunt
from django.http import HttpResponse
from django.shortcuts import render
from category.models import Category
from dish.models import Dish
from review.models import Review
from order.models import Order


class RestrauntViewSet(ModelViewSet):
    queryset = Restraunt.objects.all()
    serializer_class = RestrauntSerializer

def index(request):
    restraunts = Restraunt.objects.all()
    categories = Category.objects.all()
    return render(request, 'restraunt/index.html', {'restraunts': restraunts, 'categories': categories})

def show_restraunt_page(request, restraunt_id):
    dishes = Dish.objects.filter(restraunt_id=restraunt_id)
    restraunt = Restraunt.objects.get(id=restraunt_id)
    restraunts = Restraunt.objects.all()
    categories = Category.objects.all()
    return render(request, 'restraunt/restraunt.html', {'restraunt': restraunt, 'dishes': dishes, 
                                                        'restraunts': restraunts, 'categories': categories})

def show_reviews(request, restraunt_id):
    reviews = Review.objects.filter(restraunt_id=restraunt_id)
    restraunt = Restraunt.objects.get(id=restraunt_id)
    restraunts = Restraunt.objects.all()
    categories = Category.objects.all()
    return render(request, 'restraunt/reviews.html', {'reviews': reviews, 'restraunts': restraunts, 
                                                        'categories': categories, 'restraunt': restraunt,})

def show_order(request, order_id):
    order = Order.objects.get(id=order_id)
    restraunt = Restraunt.objects.get(id=order_id)
    dishes = Order.objects.get(id=order_id).goods.all()
    restraunts = Restraunt.objects.all()
    categories = Category.objects.all()
    return render(request, 'restraunt/cart.html', {'order': order, 'dishes': dishes, 
                                                        'categories': categories, 'restraunts': restraunts, 'restraunt': restraunt,})