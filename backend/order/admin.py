from django.contrib import admin
from order.models import Order


class OrderAdmin(admin.ModelAdmin):
    list_display = ('id', 'recipient', 'final_cost', 'restraunt')
    list_display_links = ('id', )
    search_fields = ('id', 'recipient', 'comment', 'restraunt')
    list_filter = ('final_cost', )
    filter_horizontal = ('goods',)


admin.site.register(Order, OrderAdmin)