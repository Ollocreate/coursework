from rest_framework import serializers
from order.models import Order
from dish.models import Dish
from authentication.models import User
from restraunt.models import Restraunt

class DishSerializerForOrder(serializers.ModelSerializer):
  class Meta:
    model = Dish
    fields = ['title', 'price', 'photo',]

class RecipientSerializerForOrder(serializers.ModelSerializer):
  class Meta:
    model = User
    fields = ['name', 'phone_number', 'address',]

class RestrauntSerializerForOrder(serializers.ModelSerializer):
  class Meta:
    model = Restraunt
    fields = ['title', ]

class OrderSerializer(serializers.ModelSerializer):
  recipient_data = RecipientSerializerForOrder(source="recipient")
  restraunt_data = RestrauntSerializerForOrder(source="restraunt")
  goods_data = DishSerializerForOrder(source="goods", many=True)
  class Meta:
    model = Order
    exclude = ['goods', 'recipient', 'restraunt', ]